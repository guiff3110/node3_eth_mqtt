/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/************ Debug ******************/

#define USE_DEBUG         //coment to enable serial debug
#define USE_TELE        // to use telemetry, disable by //

/************ MQTT INFORMATION (CHANGE THESE FOR YOUR SETUP) ******************/

#define mqtt_server "192.168.1.53"
#define mqtt_user "guiBroker"
#define mqtt_password "huahua209"
#define mqtt_port 1883

char message_buff[100];

const int BUFFER_SIZE = 300;

#define MQTT_MAX_PACKET_SIZE 512

/************* MQTT TOPICS (change these topics as you wish)  **************************/

 #define NODENAME "node3"
 #define topics 4
const char* state_topic[topics] = {"stat/node3/ledw", "stat/node3/out0", "stat/node6/out0", "stat/node3/led"};
const char* set_topic[topics] = {"cmnd/node3/ledw", "cmnd/node3/out0", "cmnd/node6/out0", "cmnd/node3/led"};
const char* telemetry_topic ={"tele/node3"};

/************* MQTT PAYLOADS (change these payloads as you wish)  **************************/

const char* on_cmd = "ON";
const char* off_cmd = "OFF";

/************ functions define ******************/

void callback(char* topic, byte* payload, unsigned int length);
void reconnect(void); //reconnect function
void set_output(bool OutState, int i);
void button_check(int i);
bool processJson(char* message);
void setColor(int inR, int inG, int inB, int inW, int topic);
void sendState(int topic);
void sendStateW(int topic);
void telemetry(void);
void publishData(float p_temperature);
void thermRead(void);
void save_out_status(uint8_t i);
void read_saved_out_status(uint8_t i);

/**************************** BUTTON DEFINITIONS ********************************************/

#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = { 4, 7 ,8}; // set the switch pins
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS] = {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};

/**************************** BUTTON DEFINITIONS ********************************************/

#define MAX_OUTPUT_PINS 4
const int OutputPins[MAX_OUTPUT_PINS] = {2}; // set the output pins {GPIO16, GPIO13}
volatile int OutPutState[MAX_OUTPUT_PINS]= {0, 0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2", "OUT3"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4, 4};

/**************************** TYPEDEF DEFINITIONS ********************************************/

typedef union{
  uint8_t inputs;
  struct{
    uint8_t button_0: 1,
    button_1: 1,
    button_2: 1;
  };
}inputs_typedef;

typedef union{
  uint8_t outputs;
  struct{
    uint8_t output_0: 1,
    output_1: 1,
    output_2: 1;
  };
}outputs_typedef;

typedef struct{
	inputs_typedef				inputs_s;
	outputs_typedef 			outputs_s;
	// main_machine_states_typedef	main_mach_states;
	// main_machine_states_typedef	main_mach_states_old;
	// internal_var_typedef		internal_var;
	//TODO: flag inverte o motor
	//TODO: flag triac desligado

}system_typedef;




#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
