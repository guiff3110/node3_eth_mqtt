#include <Arduino.h>
#include "main.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EEPROM.h>

/************ LAN INFORMATION (CHANGE THESE FOR YOUR SETUP) ******************/
// ethernet interface mac address, must be unique on the LAN
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 1, 20);        //Define your ip address
IPAddress gateway(192, 168, 1, 1);    //Define your gateway
IPAddress subnet(255, 255, 255, 192); //Define subnet mask

/******************************** GLOBALS DEFINITIONS *******************************/

EthernetClient ethClient;
PubSubClient client(ethClient);
system_typedef sys;

/**************************** TELEMETRY DEFINITIONS ********************************************/
#ifdef USE_TELE
unsigned long telemetry_time = 6000;
long lastMsg = 0;
float temp = 0.0;
unsigned long now = 0;
#endif

/**************************** BUTTON CHECK DEFINITIONS ********************************************/

unsigned long Button_check_time = 10;
long Button_lastTry = 0;
unsigned long Bnow = 0;

/**************************** RECONNECT DEFINITIONS ********************************************/

volatile int R = 0; //Attempt count to software reset
unsigned long reconnect_time = 1000;
long lastTry = 0;
unsigned long noww = 0;

/******************************** led *******************************/

// Maintained state for reporting to HA
byte red = 255;
byte green = 255;
byte blue = 255;
byte white = 255;
byte brightness = 255;
// Real values to write to the LEDs (ex. including brightness and state)
byte realRed = 0;
byte realGreen = 0;
byte realBlue = 0;
byte realWhite = 0;
//State of leds
bool stateOn = false;

/**************************** DEFINITIONS ********************************************/

//leds pins
const int redPin = 3;
const int greenPin = 5;
const int bluePin = 6;
const int whitePin = 9;
//LDR
const int LDR = 0;
#define LdrR 10000 // the value of the 'other' resistor
//termistor
const int thermistor = 1;
#define ThermR 10000            // the value of the 'other' resistor
#define TEMPERATURENOMINAL 25   // temp. for nominal resistance (almost always 25 C)
#define THERMISTORNOMINAL 10000 // resistance at 25 degrees C
// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 5
#define BCOEFFICIENT 4000 // The beta coefficient of the thermistor (usually 3000-4000)
uint16_t samples[NUMSAMPLES];
volatile float steinhart;

/************ void setup function ******************/

void setup()
{
  //rgb leds
  //  pinMode(redPin, OUTPUT);
  //  pinMode(greenPin, OUTPUT);
  //  pinMode(bluePin, OUTPUT);
  //  pinMode(whitePin, OUTPUT);
  //bottons
  pinMode(SW[0], INPUT);
  pinMode(SW[1], INPUT);
  pinMode(SW[2], INPUT);
  //outputs
  pinMode(OutputPins[0], OUTPUT);

  //init serial
  Serial.begin(115200);
  delay(10);

  //read last output state
  // read_saved_out_status(0);
  // read_saved_out_status(1);

  Serial.println("Starting: " + String(NODENAME));

  //Initialize ethernet
  Ethernet.begin(mac, ip, gateway, subnet);
  Serial.println("Ethernet Shield initialized.");
  if (client.connected())
  {
    Serial.print("Local IP address is:");
    Serial.println(Ethernet.localIP()); // Print Server IP address
  }
  else
    Serial.println("Connection failed");
  delay(10);
  //mqtt set connection
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  //all dependences ok
  if (!client.connected())
  {
    reconnect();
  }
  Serial.println("Ready");
#ifndef USE_DEBUG
  Serial.println("Disabling serial");
  Serial.end();
#endif
}

/************ void loop function ******************/

void loop()
{
  //check for lost connection and attempt to reconnect
  if (!client.connected())
  {
    reconnect();
  }
  //mqtt client loop
  client.loop();
  //check buttons
  Bnow = millis();
  if (Bnow - Button_lastTry > Button_check_time)
  {
    Button_lastTry = Bnow;
    for (byte i = 0; i < MAX_SWITCHS; i++)
    {
      button_check(i);
    }
  }
#ifdef USE_TELE
// telemetry();
#endif
}

/********************************** Button *****************************************/
void set_output(bool OutState, int i)
{
  save_out_status(i);
  if(OutState)
  {
    #ifdef USE_DEBUG
    Serial.print("OUTPUT ");
    Serial.print(i);
    Serial.println(" ON");
    #endif
    if(i==0){
      int topic = 0;
      OutPutState[0] = true;
      setColor(0, 0, 0, 255, topic);
      sendStateW(topic);
    }
    if(i==1){
      digitalWrite(OutputPins[2], HIGH);
     // button_led(i, false);
      OutPutState[i] = true;
      client.publish(state_topic[i], on_cmd, true);
    }
    if(i==2){
      //digitalWrite(OutputPins[i], HIGH);
      //button_led(i, true);
      OutPutState[i] = true;
      client.publish(set_topic[i], on_cmd, true);
    }
  }
  else
  {
    #ifdef USE_DEBUG
    Serial.print("OUTPUT ");
    Serial.print(i);
    Serial.println(" OFF");
    #endif
    if(i==0){
      int topic = 0;
      OutPutState[0] = false;
      setColor(0, 0, 0, 0, topic);
      sendStateW(topic);
    }
    if(i==1){
      digitalWrite(OutputPins[2], LOW);
      //button_led(i, true);
      OutPutState[i] = false;
      client.publish(state_topic[i], off_cmd, true);
    }
    if(i==2){
      //digitalWrite(OutputPins[i], LOW);
      //button_led(i, false);
      OutPutState[i] = false;
      client.publish(set_topic[i], off_cmd, true);
    }
  }
}

/********************************** Button *****************************************/
void button_check(int i)
{
  int buttonRead[MAX_SWITCHS];
  buttonRead[i] = digitalRead(SW[i]); // read the state of the switch into a local variable:
  if (buttonRead[i] != lastSwState[i])
  {
    lastDebounceTime[i] = millis(); // reset the debouncing timer
  }
  if ((millis() - lastDebounceTime[i]) > debounceDelay)
  {
    // if the button state has changed:
    if (buttonRead[i] != buttonState[i])
    {
      buttonState[i] = buttonRead[i];
      //if the button state = low, toogle LampStateOn
      if (buttonState[i] == LOW)
      {
        OutPutState[i] = !OutPutState[i];
        set_output(OutPutState[i], i);
      }
    }
  }
  lastSwState[i] = buttonRead[i];
}

/********************************** START CALLBACK*****************************************/

void callback(char *topic, byte *payload, unsigned int length)
{
#ifdef USE_DEBUG
  Serial.print("Message arrived on topic: [");
  Serial.print(topic);
  Serial.print("], msg:   ");
#endif //USE_DEBUG

  //topic to string
  String strTopic = String((char *)topic);
  strTopic.toLowerCase();

  //set_topic[0]
  if (strTopic == set_topic[0])
  {
    int topic = 0;
    char message[length + 1];
    for (unsigned int i = 0; i < length; i++)
    {
      message[i] = (char)payload[i];
    }
    message[length] = '\0';
    Serial.println(message);

    if (!processJson(message))
    {
      return;
    }
    if (stateOn)
    {
      // Update lights
      OutPutState[0] = true;
      realWhite = brightness;
      setColor(0, 0, 0, realWhite, topic);
    }
    else
    {
      realWhite = 0;
      OutPutState[0] = false;
      setColor(0, 0, 0, realWhite, topic);
    }
    sendStateW(topic);
  }

  //if set_topic[1] - output0
  if (strTopic == set_topic[1])
  {
    int x = 1;
    //payload to string
    payload[length] = '\0';
    String strPayload = String((char *)payload);
    strPayload.toUpperCase();
#ifdef USE_DEBUG
    Serial.println(strPayload);
#endif
    if (strPayload == on_cmd)
    {
      set_output(true, x);
    }
    if (strPayload == off_cmd)
    {
      set_output(false, x);
    }
  }

  //set_topic[3]
  if (strTopic == set_topic[3])
  {
    int topic = 3;
    char message[length + 1];
    for (unsigned int i = 0; i < length; i++)
    {
      message[i] = (char)payload[i];
    }
    message[length] = '\0';
#ifdef USE_DEBUG
    Serial.println(message);
#endif
    if (!processJson(message))
    {
      return;
    }
    if (stateOn)
    {
      // Update lights
      realRed = map(red, 0, 255, 0, brightness);
      realGreen = map(green, 0, 255, 0, brightness);
      realBlue = map(blue, 0, 255, 0, brightness);
      setColor(realRed, realGreen, realBlue, 0, topic);
      OutPutState[3] = true;
    }
    else
    {
      realRed = 0;
      realGreen = 0;
      realBlue = 0;
      setColor(realRed, realGreen, realBlue, 0, topic);
      OutPutState[3] = false;
    }
    sendState(topic);
  }

  // state topics

  //if set_topic[2]
  if (strTopic == state_topic[2])
  {
    int x = 2;
    //payload to string
    payload[length] = '\0';
    String strPayload = String((char *)payload);
    strPayload.toUpperCase();
#ifdef USE_DEBUG
    Serial.println(strPayload);
#endif
    if (strPayload == on_cmd)
    {
      //set_output(true, x);
      OutPutState[x] = true;
    }
    if (strPayload == off_cmd)
    {
      //set_output(false, x);
      OutPutState[x] = false;
    }
  }
}

/********************************** START RECONNECT*****************************************/

void reconnect(void)
{
  noww = millis();
  if (noww - lastTry > reconnect_time)
  {
    lastTry = noww;
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    //String clientString = "Reconnecting Arduino-" + String(Ethernet.localIP());
    // Attempt to connect
    if (client.connect(NODENAME, mqtt_user, mqtt_password))
    {
      //client.subscribe(state_topic[0]); // white led topic // button 0 topic
      //client.subscribe(state_topic[1]); // button 1 topic
      //client.subscribe(state_topic[2]); // button 2 topic
      //client.subscribe(state_topic[3]); // rgb led topic

      client.subscribe(set_topic[0]); // white led topic // button 0 topic
      client.subscribe(set_topic[1]); // button 1 topic
      client.subscribe(state_topic[2]); // button 2 topic
      client.subscribe(set_topic[3]); // rgb led topic

      Serial.println("connected");
      Serial.println("Node name:" NODENAME);
      Serial.print("IP address: ");
      Serial.println(Ethernet.localIP()); // Print Server IP address

      R = 0;
      //setColor(0, 0, 0, 0, 3);
      //sendState(3);
      //setColor(0, 0, 0, 0, 0);
      //sendStateW(0);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again ");
      R++;
      Serial.print("Retry count: ");
      Serial.println(R);
      if (R >= 10)
      {
        Serial.println("FUUU");
      }
    }
  }
}

/********************************** telemtry *****************************************/
#ifdef USE_TELE
void telemetry(void)
{
  now = millis();
  if (now - lastMsg > telemetry_time)
  {
    lastMsg = now;
    //publishData(sensor.temp());
    int lux = 0;
    lux = analogRead(LDR);
    Serial.print("Lux: ");
    Serial.print(lux);
    thermRead();
  }
}
#endif

/********************************** thermRead *****************************************/

void thermRead(void)
{
  uint8_t i;
  float average;

  // take N samples in a row, with a slight delay
  for (i = 0; i < NUMSAMPLES; i++)
  {
    samples[i] = analogRead(thermistor);
    delay(10);
  }

  // average all the samples out
  average = 0;
  for (i = 0; i < NUMSAMPLES; i++)
  {
    average += samples[i];
  }
  average /= NUMSAMPLES;

  // convert the value to resistance
  average = 1023 / average - 1;
  average = ThermR / average;
  steinhart = average / THERMISTORNOMINAL;          // (R/Ro)
  steinhart = log(steinhart);                       // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                        // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                      // Invert
  steinhart -= 273.15;
  Serial.print("Temperature: ");
  Serial.println(steinhart);
}

/********************************** Json temperature*****************************************/
#ifdef USE_TELE
void publishData(float p_temperature)
{
  // create a JSON object
  // doc : https://github.com/bblanchon/ArduinoJson/wiki/API%20Reference
  StaticJsonDocument<200> doc;
  // JsonObject root = doc.to<JsonObject>();
  // INFO: the data must be converted into a string; a problem occurs when using floats...
  doc["temperature"] = (String)p_temperature;
#ifdef USE_DEBUG
  serializeJson(doc, Serial);
  Serial.println("");
/*
     {
        "temperature": "23.20" ,
        "humidity": "43.70"
     }
  */
#endif //USE_DEBUG
  char data[200];
  serializeJson(doc, data, measureJson(doc) + 1);
  client.publish(telemetry_topic, data, true);
}
#endif

/********************************** START PROCESS JSON*****************************************/

bool processJson(char *message)
{

  StaticJsonDocument<BUFFER_SIZE> doc;
  JsonObject root = doc.to<JsonObject>();
  auto error = deserializeJson(doc, message);

  if (error)
  {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());
    return false;
  }

  if (root.containsKey("state"))
  {
    if (strcmp(root["state"], on_cmd) == 0)
    {
      stateOn = true;
    }
    else if (strcmp(root["state"], off_cmd) == 0)
    {
      stateOn = false;
    }
  }
  if (root.containsKey("color"))
  {
    red = root["color"]["r"];
    green = root["color"]["g"];
    blue = root["color"]["b"];
  }

  if (root.containsKey("brightness"))
  {
    brightness = root["brightness"];
  }

  return true;
}

/********************************** START SEND STATE*****************************************/

void sendState(int topic)
{
  // StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  StaticJsonDocument<BUFFER_SIZE> doc;

  JsonObject root = doc.to<JsonObject>();

  root["state"] = (OutPutState[3]) ? on_cmd : off_cmd;
  JsonObject color = root.createNestedObject("color");
  color["r"] = red;
  color["g"] = green;
  color["b"] = blue;

  root["brightness"] = brightness;

  char buffer[measureJson(doc) + 1];
  serializeJson(doc, buffer, sizeof(buffer));
#ifdef USE_DEBUG
  Serial.println(buffer);
#endif
  client.publish(state_topic[topic], buffer, true);
}

/********************************** START SEND STATE*****************************************/

void sendStateW(int topic)
{

  StaticJsonDocument<BUFFER_SIZE> doc;

  //doc["state"] = (stateOn) ? on_cmd : off_cmd;
  doc["state"] = (OutPutState[0]) ? on_cmd : off_cmd;

  doc["brightness"] = brightness;

  char buffer[measureJson(doc) + 1];
  serializeJson(doc, buffer, sizeof(buffer));
#ifdef USE_DEBUG
  Serial.println(buffer);
#endif
  client.publish(state_topic[topic], buffer, true);
}

/********************************** START SET COLOR *****************************************/
void setColor(int inR, int inG, int inB, int inW, int topic)
{

  if (topic == 3)
  {
    //  analogWrite(redPin, inR);
    //  analogWrite(greenPin, inG);
    //  analogWrite(bluePin, inB);

#ifdef USE_DEBUG
    Serial.println("Setting LEDs:");
    Serial.print("r: ");
    Serial.print(inR);
    Serial.print(", g: ");
    Serial.print(inG);
    Serial.print(", b: ");
    Serial.print(inB);
#endif //USE_DEBUG
  }

  if (topic == 0)
  {
    //  analogWrite(bluePin, inW);

#ifdef USE_DEBUG
    Serial.println("Setting LEDs:");
    Serial.print(" w: ");
    Serial.println(inW);
#endif //USE_DEBUG
  }
}

/********************************** EEPROM SAVE OUTPUT STATE *****************************************/

void save_out_status(uint8_t i)
{
  //EEPROM.begin(allocated_mem[i]);
  EEPROM.write(0, OutPutState[i]);
#ifdef USE_DEBUG
  Serial.print(" Save to EEPROM ");
  Serial.println(i);
  Serial.println(OutPutState[i]);
#endif
  EEPROM.end();
}

/********************************** EEPROM READ LAST OUTPUT STATE *****************************************/

void read_saved_out_status(uint8_t i)
{
  //EEPROM.begin(allocated_mem[i]);
  OutPutState[i] = EEPROM.read(0); //read value to EEPROM and set status
  Serial.println(OutPutState[i]);
  if (OutPutState[i] > 1)
  {
    OutPutState[i] = 1; //if a first read from EEPROM
    EEPROM.write(0, OutPutState[i]);
  }
#ifdef USE_DEBUG
  Serial.print(" Last output state read to EEPROM ");
  Serial.print(i);
  Serial.println(OutPutState[i]);
#endif
  set_output(OutPutState[i], i);
  EEPROM.end();
}
