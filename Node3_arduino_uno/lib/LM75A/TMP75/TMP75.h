/*
    TMP75 - An arduino library for the TMP75 temperature sensor
    Copyright (C) 2011  Gui FF <guiff3110 AT gmail DOT com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Datasheet http://www.ti.com/lit/ds/symlink/tmp75.pdf
 */

#ifndef TMP75_h

#define TMP75_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define TMP75_ADDRESS0 0x48 // A2 - GND, A1 - GND, A0 - GND - 1001000
#define TMP75_ADDRESS1 0x49 // A2 - GND, A1 - GND, A0 - VCC - 1001001
#define TMP75_ADDRESS2 0x4A // A2 - GND, A1 - VCC, A0 - GND - 1001010
#define TMP75_ADDRESS3 0x4B // A2 - GND, A1 - VCC, A0 - VCC - 1001011
#define TMP75_ADDRESS4 0x4C // A2 - VCC, A1 - GND, A0 - GND - 1001100
#define TMP75_ADDRESS5 0x4D // A2 - VCC, A1 - GND, A0 - VCC - 1001101
#define TMP75_ADDRESS6 0x4E // A2 - VCC, A1 - VCC, A0 - GND - 1001110
#define TMP75_ADDRESS7 0x4F // A2 - VCC, A1 - VCC, A0 - VCC - 1001111

#define TMP75_TEMP_REGISTER 0
#define TMP75_CONF_REGISTER 1
#define TMP75_TLOW_REGISTER 2
#define TMP75_THIGH_REGISTER 3

#define TMP75_CONF_SHUTDOWN  0
#define TMP75_CONF_THERMOSTAT_MODE  1
#define TMP75_CONF_POLARITY 2
#define TMP75_CONF_FAULT_QUEUE0 3
#define TMP75_CONF_FAULT_QUEUE1 4
#define TMP75_CONF_CONV_RES0 5
#define TMP75_CONF_CONV_RES1 6
#define TMP75_CONF_ONE_SHOT 7


class TMP75 {
    int address;
    word float2regdata (float);
    float regdata2float (word);
    word _register16 (byte);
    void _register16 (byte, word);
    word _register8 (byte);
    void _register8 (byte, byte);
  public:
    TMP75 ();
    TMP75 (byte);
    float temp (void);
    byte conf (void);
    void conf (byte);
    float tHigh (void);
    void tHigh (float);
    float tLow (void);
    void tLow (float);
    void shutdown (boolean);
    boolean shutdown (void);
};

#endif
