/*
    TMP75 - An arduino library for the TMP75 temperature sensor
    Copyright (C) 2011  Gui FF <guiff3110 AT gmail DOT com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Wire.h>
#include "TMP75.h"

TMP75::TMP75 (byte addr) {
  address = addr;
}

word TMP75::float2regdata (float temp)
{
  // First multiply by 8 and coerce to integer to get +/- whole numbers
  // Then coerce to word and bitshift 5 to fill out MSB
  return (word)((int)(temp * 8) << 5);
}

float TMP75::regdata2float (word regdata)
{
  return ((float)(int)regdata / 32) / 8;
}

word TMP75::_register16 (byte reg) {
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(address, 2);
  word regdata = (Wire.read() << 8) | Wire.read();
  return regdata;
}

void TMP75::_register16 (byte reg, word regdata) {
  byte msb = (byte)(regdata >> 8);
  byte lsb = (byte)(regdata);

  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.write(msb);
  Wire.write(lsb);
  Wire.endTransmission();
}

word TMP75::_register8 (byte reg) {
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(address, 1);
  return Wire.read();
}

void TMP75::_register8 (byte reg, byte regdata) {
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.write(regdata);
  Wire.endTransmission();
}

float TMP75::temp (void) {
  return regdata2float(_register16(TMP75_TEMP_REGISTER));
}

byte TMP75::conf () {
  return _register8(TMP75_CONF_REGISTER);
}

void TMP75::conf (byte data) {
  _register8(TMP75_CONF_REGISTER, data);
}

float TMP75::tHigh () {
  return regdata2float(_register16(TMP75_THIGH_REGISTER));
}

void TMP75::tHigh (float temp) {
  _register16(TMP75_THIGH_REGISTER, float2regdata(temp));
}

float TMP75::tLow () {
  return regdata2float(_register16(TMP75_TLOW_REGISTER));
}

void TMP75::tLow (float temp) {
  _register16(TMP75_TLOW_REGISTER, float2regdata(temp));
}

boolean TMP75::shutdown () {
  return conf() & 0x01;
}

void TMP75::shutdown (boolean val) {
  conf(val << TMP75_CONF_SHUTDOWN);
}
